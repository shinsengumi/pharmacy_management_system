from django.http import HttpResponse
from django.shortcuts import render, redirect
from new_app.models import  Medicine, MedicineInvoices, Invoice
from datetime import datetime


def checkout(request):
    # print("access is on")
    new_create = Invoice.objects.filter(is_sold=False)
    # new_create = Invoice.objects.get(is_sold=False)  # Trying to Get Invoice object
    # objects = Medicine()

    if len(new_create) == 0:
        temp = 0
        return render(request, 'checkout.html', {'temp': temp})

    else:
        new_create = Invoice.objects.get(is_sold=False)
        objs = []
        invoice_obj_all = new_create.medicine_invoice.all()
        summ = 0.0
        for obj in invoice_obj_all:
            objs.append(obj.medicine)
            summ = summ + obj.noOfUnit * obj.medicine.price

        summm = str(summ)
        zipped_data = zip(objs, invoice_obj_all)


        if request.method == "POST":

            man_id = request.POST.get('ano_id')
            # print("hi there")

            for obj in invoice_obj_all:
                if str(obj.id) == man_id:
                    new_create.medicine_invoice.remove(obj)
                    obj.delete()

            new_create = Invoice.objects.get(is_sold=False)
            objs = []
            invoice_obj_all = new_create.medicine_invoice.all()

            summ = 0.0
            for obj in invoice_obj_all:
                objs.append(obj.medicine)
                summ = summ + obj.noOfUnit * obj.medicine.price

            summm = str(summ)

            zipped_data = zip(objs, invoice_obj_all)


            if len(invoice_obj_all) == 0:
                temp = 0
                new_create.delete()
            else:
                temp = 1

            return render(request, 'checkout.html',
                          {'new_create': new_create, 'objs': objs, 'invoice_obj_all': invoice_obj_all,
                           'zipped_data': zipped_data, 'temp': temp, 'summm': summm})

        if request.is_ajax() and request.method == 'GET' and request.GET['here'] == 'not_finish':
            medi_name = request.GET['medi_name']
            amount_of_medi = request.GET['amount_of_medi']
            for obj in invoice_obj_all:
                if obj.medicine.medicineName == medi_name:
                    a = int('0' + amount_of_medi)
                    obj.noOfUnit = a
                    obj.save()

        if request.is_ajax() and request.method == 'GET' and request.GET['here'] == 'finish':
            # print("this is ajax requ")
            for obj1, obj2 in zipped_data:
                obj1.amount -= obj2.noOfUnit
                obj1.save()
                obj2.cond = True
                obj2.save()
            new_create = Invoice.objects.get(is_sold=False)
            invoice_obj_all = new_create.medicine_invoice.all()
            summ = 0.0
            for obj in invoice_obj_all:
                summ = summ + obj.noOfUnit * obj.medicine.price
                obj.individual_price = obj.medicine.price
                obj.save()
            new_create.is_sold = True
            new_create.sellingTime = datetime.now().time()
            new_create.sellingDate = datetime.now().date()
            new_create.selling_price = summ
            new_create.save()

            return redirect('/home')

        temp = 1

        return render(request, 'checkout.html',
                      {'new_create': new_create, 'objs': objs, 'invoice_obj_all': invoice_obj_all,
                       'zipped_data': zipped_data, 'temp': temp, 'summm': summm})

