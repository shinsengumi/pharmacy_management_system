from django import forms
from django.contrib.admin.widgets import RelatedFieldWidgetWrapper
from Pharmacy.widget import DataAttributesSelect
from new_app.models import Medicine, GenericName, MedicineTypeOnTake, Brand, Employee

YEARS = [x for x in range(2019, 2040)]


class LoginForm(forms.Form):
    name = forms.CharField(
        widget=forms.TextInput(attrs={'label': 'Username', 'class': 'form-control', 'placeholder': 'Enter Name'}))
    password = forms.CharField(label='Password', widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Enter Password', 'type': 'password'}))

    def clean(self, *args, **kwargs):
        # print("I am here")
        user_name = self.cleaned_data.get("name")
        user_password = self.cleaned_data.get("password")
        # print(user_password)
        # if Employee.objects.filter(name=user_name).exists():
        user_obj = Employee.objects.filter(name=user_name).first()
        #print(user_obj.user_pass)
        # user_p = getattr(user_obj, 'password')
        # print(user_p)
        if not user_obj:
            print("No")
            raise forms.ValidationError("Invalid Username")
        else:
            if user_password != getattr(user_obj, 'password'):
                raise forms.ValidationError("Password Does not Match with username")

        # else:
        #     raise forms.ValidationError("Invalid Username")

        return user_name


class MedicineModelForm(forms.ModelForm):
    # long_desc = forms.CharField(widget=forms.TextInput(attrs={'cols': 10, 'rows': 20}))
    # short_desc = forms.CharField(widget=forms.Textarea)
    class Meta:
        model = Medicine
        fields = ["medicineName",
                  "genre",
                  "medicineType",
                  "brand",
                  "amount",
                  "price",
                  "description"]

    def __init__(self, *args, **kwargs):
        super(MedicineModelForm, self).__init__(*args, **kwargs)
        self.fields['medicineName'].widget = forms.TextInput(
            attrs={'label': 'Medicine Name', 'class': 'form-control', 'placeholder': 'Enter Name'})

        # object = GenericName.objects.values_list('genreName')  # values_list('genreName')
        # ch = (("1", "one"), ("2", "two"), )
        # print(ch)
        self.fields['genre'] = forms.ModelChoiceField(label="Genre", queryset=GenericName.objects.all(),
                                                      widget=forms.Select(attrs={'class': 'regDropDown'}))

        self.fields['medicineType'] = forms.ModelChoiceField(label='Medicine Type',
                                                             queryset=MedicineTypeOnTake.objects.all(),
                                                             widget=forms.Select(attrs={'class': 'regDropDown'}))

        self.fields['brand'] = forms.ModelChoiceField(
            label='Brand', queryset=Brand.objects.all(),
            widget=forms.Select(attrs={'class': 'regDropDown'}))

        self.fields['amount'].widget = forms.TextInput(
            attrs={'label': 'Medicine Amount', 'class': 'form-control', 'placeholder': 'Enter amount of medicine'})

        self.fields['price'].widget = forms.TextInput(
            attrs={'label': 'Price', 'class': 'form-control', 'placeholder': 'Enter price'})

        self.fields['description'].widget = forms.Textarea(
            attrs={'label': 'Description', 'class': 'form-control', 'placeholder': 'Medicine Description'})


class ImportMedForm(forms.Form):
    medicine = forms.CharField(widget=forms.TextInput(attrs={'label': "Medicine's Name", 'class': 'form-control',
                                                             'placeholder': "Enter Medicin's Name"}))
    # generic_name = forms.CharField(
    #     widget=forms.TextInput(attrs={'label': "Medicine's Generic Name", 'class': 'form-control',
    #                                   'placeholder': "Enter Medicin's Generic Name"}))

    generic_name = forms.ModelChoiceField(label="Generic name", queryset=GenericName.objects.only("genreName"),
                                          widget=forms.Select(attrs={'class': 'regDropDown'}), )

    manufacturer = forms.ModelChoiceField(label='Brand', queryset=Brand.objects.only("brand"),
                                          widget=forms.Select(attrs={'class': 'regDropDown'}))
    taking_method = forms.ModelChoiceField(label=' How to take',
                                           queryset=MedicineTypeOnTake.objects.only("intakeMethod"),
                                           widget=forms.Select(attrs={'class': 'regDropDown'}))

    no_of_unit = forms.IntegerField(widget=forms.TextInput(attrs={'label': "NO. of unit", 'class': 'form-control',
                                                                  'placeholder': "NO. of unit"}))
    size = forms.IntegerField(widget=forms.TextInput(attrs={'label': "Power of medicine", 'class': 'form-control',
                                                            'placeholder': "x (mg/ ml)"}))
    buying_price_per_unit = forms.FloatField(
        widget=forms.TextInput(attrs={'label': "Buying price per unit", 'class': 'form-control',
                                      'placeholder': "Buying price per unit"}))
    date_Of_Manufacture = forms.DateField(label="Date of Manufacture\t", widget=forms.SelectDateWidget(years=YEARS))
    date_Of_Expire = forms.DateField(label="Date of Expiration\t\t", widget=forms.SelectDateWidget(years=YEARS))
    recommendedDose = forms.CharField(required=True,
                                      widget=forms.TextInput(
                                          attrs={'label': "Recommended Dose", 'class': 'form-control',
                                                 'placeholder': "Doses"}))
    instructions = forms.CharField(required=False,
                                   widget=forms.TextInput(
                                       attrs={'label': "Additional instructions", 'class': 'form-control',
                                              'placeholder': ""}))

# class MyModelAdminForm(forms.ModelForm):
#     class Meta:
#         model = Medicine
#         fields = ["genre"]
#
#     def __init__(self, *args, **kwargs):
#         super(MyModelAdminForm, self).__init__(*args, **kwargs)
#
#         data = {'data-foo': dict(GenericName.objects.values_list('id', 'genreName'))}
#         data['data-foo'][''] = ''  # empty option
#
#         self.fields['genre'].widget = RelatedFieldWidgetWrapper(DataAttributesSelect(
#             choices=self.fields['genre'].choices,
#             data=data
#         ),
#         #     GenericName._meta.get_fields(include_hidden=True).rel,
#         #     # self.fields['genre'].widget.rel,
#         #     self.fields['genre'].widget.admin_site,
#         #     # self.fields['genre'].widget.can_add_related,
#         #     # self.fields['genre'].widget.can_change_related,
#         #     # self.fields['genre'].widget.can_delete_related,
#         #
#         # )
