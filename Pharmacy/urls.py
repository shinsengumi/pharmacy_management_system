from django.conf.urls import url, include
from django.contrib import admin

from . import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^login/', views.login),
    url(r'^logout/', views.logout),
    url(r'^home/', views.home),
    url(r'^adminHome/', views.adminHome),
    url(r'^dailyreport/', views.dailyReport),
    url(r'^medicines/', views.medicines),
    url(r'^add_medicines/', views.add_medicines),
    url(r'^employees/', views.employees),
    url(r'^notification/', views.notification),
    url(r'^dealers/', views.dealers),
    url(r'^checkout/', include('Cart.urls')),
    url(r'^import/', views.importMed),
    url(r'^trade_alphabetical/(?P<string>[\w\-]+)/$', views.Trade_Search),
    url(r'^generic_alphabetical/(?P<string>[\w\-]+)/$', views.Generic_Search),
    url(r'^history/', views.History),
    url(r'^invoice-details/(?P<int>[\w\-]+)/$', views.Invoice_Details),
    url(r'^search/', views.Search, name='search'),
    # url(r'^test/', views.test),
    url(r'^$', views.login),
    url(r'^edit/(?P<int>[\w\-]+)/$', views.Edit),
    url(r'^bestreports/', views.Medicine_Reports),

]
