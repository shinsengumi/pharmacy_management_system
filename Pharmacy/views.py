from datetime import datetime, date

from django.http import HttpResponse
import re
from django.shortcuts import render, redirect
from django.db.models import Sum
from django.urls import reverse
import decimal

from . import forms
from new_app.models import Medicine, MedicineInvoices, Employee, Invoice, Salesman, GenericName, \
    BackOffice, Supply, MedicineBatch, Brand, MedicineTypeOnTake
from django.db import connection, transaction
import datetime as DT

eID = -1  # type: int


def login(request):
    global eID
    if eID != -1:
        return redirect('/home/')
    login_form = forms.LoginForm()
    if request.method == "POST":
        login_form = forms.LoginForm(request.POST)
        # print(print(request.POST.get("password")))
        if login_form.is_valid():

            # print(login_form['name'].value())
            # print("One")
            eID = Employee.objects.only("id").filter(name=login_form['name'].value(),
                                                     password=login_form['password'].value())
            print(eID)
            if login_form['name'].value() == "admin":
                return redirect('/adminHome/')
        return redirect('/home/')

    return render(request, 'login.html', {'login_form': login_form})


def logout(request):
    global eID
    eID = -1
    login_form = forms.LoginForm()
    if request.method == "POST":
        login_form = forms.LoginForm(request.POST)
        # print(print(request.POST.get("password")))
        if login_form.is_valid():

            # print(login_form['name'].value())
            # print("One")
            eID = Employee.objects.only("id").filter(name=login_form['name'].value(),
                                                     password=login_form['password'].value())
            print(eID)
            if login_form['name'].value() == "admin":
                return redirect('/adminHome/')
        return redirect('/home/')

    return render(request, 'login.html', {'login_form': login_form})


def home(request):
    med_name = []
    med_amount = []

    temp = MedicineInvoices.objects.values('medicine').annotate(sum=Sum('noOfUnit')).order_by('-sum')
    # print(temp.count())
    # print(Medicine.objects.only('medicineName').filter(id=temp[0]['medicine']))
    # print(temp[0]['sum'])
    u = 6;
    if temp.count() < 6:
        u = temp.count;
    for x in range(0, min(temp.count(), 6)):
        med_name.append(Medicine.objects.get(id=temp[x]['medicine']))
        med_amount.append(temp[x]['sum'])

    # zipped_data = zip(med_name, med_amount)
    #
    # for obj1,obj2 in zipped_data:
    #     print(obj1)
    #     print(obj2)
    ideal = "hello"

    # print(med_name[2])

    return render(request, 'home.html', {'med_name': med_name, 'med_amount': med_amount, 'ideal': ideal})


def adminHome(request):
    med_name = []
    med_amount = []

    temp = MedicineInvoices.objects.values('medicine').annotate(sum=Sum('noOfUnit')).order_by('-sum')
    # print(temp.count())
    # print(Medicine.objects.only('medicineName').filter(id=temp[0]['medicine']))
    # print(temp[0]['sum'])
    u = 6;
    if temp.count() < 6:
        u = temp.count;
    for x in range(0, min(temp.count(), 6)):
        med_name.append(Medicine.objects.get(id=temp[x]['medicine']))
        med_amount.append(temp[x]['sum'])

    # zipped_data = zip(med_name, med_amount)
    #
    # for obj1,obj2 in zipped_data:
    #     print(obj1)
    #     print(obj2)
    ideal = "hello"

    # print(med_name[2])
    return render(request, 'adminHome.html', {'med_name': med_name, 'med_amount': med_amount, 'ideal': ideal})


def medicines(request):
    new_create = 0
    if request.is_ajax():
        med_id = request.GET['post_med_id']
        if med_id is not None:
            medicine_obj = Medicine.objects.get(id=med_id)  # Get medicine object

            try:
                new_create = Invoice.objects.get(is_sold=False)  # Trying to Get Invoice object
            except Invoice.DoesNotExist:
                salesman = Salesman.objects.get(counterNo=1)  # Get Salesman object
                new_create = Invoice.objects.create(employee=salesman)  # Creating new Invoice object
            tt = 0
            invoice_obj_all = new_create.medicine_invoice.all()

            for obj in invoice_obj_all:
                temp_med = Medicine.objects.get(medicineName=obj.medicine)
                if temp_med.medicineName == medicine_obj.medicineName:
                    tt = 1

            if tt == 0:
                print("i am here")
                medicine_invoice_obj = MedicineInvoices.objects.create(medicine=medicine_obj)
                new_create.medicine_invoice.add(medicine_invoice_obj)
                temp = new_create.medicine_invoice.all()

    new_create = Invoice.objects.filter(is_sold=False)
    is_med_in = []

    if len(new_create) != 0:
        new_create = Invoice.objects.get(is_sold=False)
        invoice_obj_all = new_create.medicine_invoice.all()
        for obj in invoice_obj_all:
            is_med_in.append(obj.medicine)

    user_obj = Medicine.objects.order_by('medicineName')
    return render(request, 'medicines.html', {'user_obj': user_obj, 'is_med_in': is_med_in})


def add_medicines(request):
    form = forms.MedicineModelForm()
    if request.method == "POST":
        form = forms.MedicineModelForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/medicines/')
        print(form)
    return render(request, 'add_medicines.html', {'form': form})


def notification(request):
    all_med = Medicine.objects.all()

    med_obj = []
    for obj in all_med:
        if obj.amount < obj.limit:
            med_obj.append(obj)

    return render(request, 'notification.html', {'med_obj': med_obj})


def dealers(request):
    info = Brand.objects.values('brand', 'contactNo')
    for i in info:
        print(i["brand"])
    return render(request, 'dealers.html', {'info': info})


def importMed(request):
    form = forms.ImportMedForm()
    if request.method == 'POST':
        form = forms.ImportMedForm(request.POST)
        if form.is_valid():
            # if BackOffice.objects.get(employee=1).exists():
            b = BackOffice.objects.get(employee=1)
            # if brand = Brand.objects.filter(bra).exists():
            m = Brand.objects.get(brand=form.cleaned_data['manufacturer']);
            mType = MedicineTypeOnTake.objects.get(intakeMethod=form.cleaned_data['taking_method']);
            s = Supply.objects.create(brand=form.cleaned_data['manufacturer'], employee=b)
            if GenericName.objects.filter(genreName=form.cleaned_data['generic_name']).exists():
                g = GenericName.objects.get(genreName=form.cleaned_data['generic_name'])
            else:
                g = GenericName.objects.create(genreName=form.cleaned_data['generic_name'])

            if Medicine.objects.filter(medicineName=form.cleaned_data['medicine'],
                                       medicineType=mType, size=form.cleaned_data['size'], brand=m).exists():
                med = Medicine.objects.get(medicineName=form.cleaned_data['medicine'])
            else:
                med = Medicine.objects.create(medicineName=form.cleaned_data['medicine'],
                                              price=form.cleaned_data['buying_price_per_unit'] * 1.02,
                                              description=(form.cleaned_data['medicine']),
                                              size=form.cleaned_data['size'],
                                              instructions=form.cleaned_data['instructions'],
                                              recDose=form.cleaned_data['recommendedDose'],
                                              amount=form.cleaned_data['no_of_unit'],
                                              medicineType=mType,
                                              brand=m,
                                              limit=40,
                                              genre=g)
            MedicineBatch.objects.create(noOfUnit=form.cleaned_data['no_of_unit'],
                                         buyingPrice=form.cleaned_data['buying_price_per_unit'],
                                         dateOfManufacture=form.cleaned_data['date_Of_Manufacture'],
                                         dateOfExpire=form.cleaned_data['date_Of_Expire'], medicine=med, supply=s)
            # total = form.cleaned_data['no_of_unit'] + m.amount
            # m.amount = total
            return redirect('/medicines/')
    return render(request, 'importMed.html', {'form': form})


def Trade_Search(request, string=None):
    new_create = 0
    text = string
    text = re.escape(text)  # make sure there are not regex specials
    if request.is_ajax():
        med_id = request.GET['post_med_id']
        if med_id is not None:
            medicine_obj = Medicine.objects.get(id=med_id)  # Get medicine object

            try:
                new_create = Invoice.objects.get(is_sold=False)  # Trying to Get Invoice object
            except Invoice.DoesNotExist:
                salesman = Salesman.objects.get(counterNo=1)  # Get Salesman object
                new_create = Invoice.objects.create(employee=salesman)  # Creating new Invoice object
            tt = 0
            invoice_obj_all = new_create.medicine_invoice.all()

            for obj in invoice_obj_all:
                temp_med = Medicine.objects.get(medicineName=obj.medicine)
                if temp_med.medicineName == medicine_obj.medicineName:
                    tt = 1

            if tt == 0:
                medicine_invoice_obj = MedicineInvoices.objects.create(medicine=medicine_obj)
                new_create.medicine_invoice.add(medicine_invoice_obj)
                temp = new_create.medicine_invoice.all()
                print(temp)

    new_create = Invoice.objects.filter(is_sold=False)
    is_med_in = []

    if len(new_create) != 0:
        new_create = Invoice.objects.get(is_sold=False)
        invoice_obj_all = new_create.medicine_invoice.all()
        for obj in invoice_obj_all:
            is_med_in.append(obj.medicine)

    user_obj = Medicine.objects.filter(medicineName__startswith=text)
    # print(text)
    return render(request, 'trade_alpha.html', {'user_obj': user_obj, 'is_med_in': is_med_in, 'text': text})


def Generic_Search(request, string=None):
    new_create = 0
    text = string
    text = re.escape(text)  # make sure there are not regex specials
    if request.is_ajax():
        med_id = request.GET['post_med_id']
        if med_id is not None:
            medicine_obj = Medicine.objects.get(id=med_id)  # Get medicine object

            try:
                new_create = Invoice.objects.get(is_sold=False)  # Trying to Get Invoice object
            except Invoice.DoesNotExist:
                salesman = Salesman.objects.get(counterNo=1)  # Get Salesman object
                new_create = Invoice.objects.create(employee=salesman)  # Creating new Invoice object
            tt = 0
            invoice_obj_all = new_create.medicine_invoice.all()

            for obj in invoice_obj_all:
                temp_med = Medicine.objects.get(medicineName=obj.medicine)
                if temp_med.medicineName == medicine_obj.medicineName:
                    tt = 1

            if tt == 0:
                medicine_invoice_obj = MedicineInvoices.objects.create(medicine=medicine_obj)
                new_create.medicine_invoice.add(medicine_invoice_obj)
                temp = new_create.medicine_invoice.all()
                print(temp)

    new_create = Invoice.objects.filter(is_sold=False)
    is_med_in = []

    if len(new_create) != 0:
        new_create = Invoice.objects.get(is_sold=False)
        invoice_obj_all = new_create.medicine_invoice.all()
        for obj in invoice_obj_all:
            is_med_in.append(obj.medicine)

    text = string
    text = re.escape(text)  # make sure there are not regex specials
    med_objs = []
    type_objs = GenericName.objects.filter(genreName__startswith=text)
    xxx = Medicine.objects.all()

    for x in xxx:
        if x.genre in type_objs:
            med_objs.append(x)

    return render(request, 'generic_alpha.html', {'user_obj': med_objs, 'is_med_in': is_med_in, 'text': text})


def History(request):
    total_invoice = Invoice.objects.filter(is_sold=True).order_by('-sellingDate', '-sellingTime')
    print(total_invoice)
    return render(request, 'history.html', {'total_invoice': total_invoice})


def Invoice_Details(request, int=None):
    here = int
    invoice_object = Invoice.objects.get(id=here)
    invoice_obj_all = invoice_object.medicine_invoice.all()
    cost = invoice_object.selling_price

    for_every_sold_med = []
    for x in invoice_obj_all:
        for_every_sold_med.append(x.noOfUnit * x.medicine.price)

    print(invoice_obj_all)
    zipped_data = zip(invoice_obj_all, for_every_sold_med)

    return render(request, 'detail_invoice.html', {'cost': cost, 'zipped_data': zipped_data})


def Test(request):
    total_invoice = Invoice.objects.filter(is_sold=True).order_by('-sellingDate', '-sellingTime')
    print(total_invoice)
    return render(request, 'new_test.html', {})


def dailyReport(request):
    total_invoice = Invoice.objects.filter(is_sold=True, sellingDate=date.today())
    print(total_invoice)
    dict = {}
    for inv in total_invoice:
        medicinesInv = inv.medicine_invoice.all()
        print(medicinesInv)
        for mInv in medicinesInv:
            if getattr(mInv, "medicine") in dict:
                dict[getattr(mInv, "medicine")] = dict[getattr(mInv, "medicine")] + getattr(mInv, "noOfUnit")
            else:
                dict[getattr(mInv, "medicine")] = getattr(mInv, "noOfUnit")
    print(dict)
    for x in dict:
        print(getattr(x, "brand"))
    dummy = "one"

    dictS = sorted(dict.items(), key=lambda x: x[1], reverse=True)
    print(dictS)

    return render(request, "dailyreport.html",
                  {'dict': dict, 'dummy': dummy})


def employees(request):
    dummy = "one"
    # print(employeeInfo)
    dic = {}
    for employee in Employee.objects.all().only("id", 'name', "mobileNo", "emailId", "branch"):
        # print(e)
        # print(type(employee))
        if Salesman.objects.filter(employee=employee.id).exists():
            dic[employee] = "Salesman"
        else:
            dic[employee] = BackOffice.objects.only("designation").filter(employee=getattr(employee, "id"))
    for d in dic:
        print(d)
        print(dic[d])
    return render(request, "employee.html", {'dict': dic, 'dummy': dummy})


def Search(request):

    if request.is_ajax():
        med_id = request.GET['post_med_id']
        if med_id is not None:
            medicine_obj = Medicine.objects.get(id=med_id)  # Get medicine object

            try:
                new_create = Invoice.objects.get(is_sold=False)  # Trying to Get Invoice object
            except Invoice.DoesNotExist:
                salesman = Salesman.objects.get(counterNo=3)  # Get Salesman object
                new_create = Invoice.objects.create(employee=salesman)  # Creating new Invoice object
            tt = 0
            invoice_obj_all = new_create.medicine_invoice.all()


            for obj in invoice_obj_all:
                temp_med = Medicine.objects.get(medicineName=obj.medicine)
                if temp_med.medicineName == medicine_obj.medicineName:
                    tt = 1

            if tt == 0:
                medicine_invoice_obj = MedicineInvoices.objects.create(medicine=medicine_obj)
                new_create.medicine_invoice.add(medicine_invoice_obj)
                temp = new_create.medicine_invoice.all()
                print(temp)

    if request.method == "POST":
        search_type  = request.POST.get('search_param')
        search_content = request.POST.get('x')

        user_obj = 0

        if search_type == "contains":

            new_create = Invoice.objects.filter(is_sold=False)
            is_med_in = []

            if len(new_create) != 0:
                new_create = Invoice.objects.get(is_sold=False)
                invoice_obj_all = new_create.medicine_invoice.all()
                for obj in invoice_obj_all:
                    is_med_in.append(obj.medicine)



            search_content = re.escape(search_content)
            user_obj = Medicine.objects.filter(medicineName__startswith=search_content)
            # print(type_objs)

        if search_type == "its_equal":

            new_create = Invoice.objects.filter(is_sold=False)
            is_med_in = []

            if len(new_create) != 0:
                new_create = Invoice.objects.get(is_sold=False)
                invoice_obj_all = new_create.medicine_invoice.all()
                for obj in invoice_obj_all:
                    is_med_in.append(obj.medicine)


            search_content = re.escape(search_content)  # make sure there are not regex specials
            user_obj = []
            type_objs = GenericName.objects.filter(genreName__startswith=search_content)
            xxx = Medicine.objects.all()

            for x in xxx:
                if x.genre in type_objs:
                    user_obj.append(x)

    return render(request, 'search.html', {'user_obj': user_obj, 'is_med_in': is_med_in})




def Edit(request, int=None):
    here = int
    here = str(here)



    med_object = Medicine.objects.get(id=here)
    limit = med_object.limit
    descrip = med_object.description
    cos = med_object.price


    # Start Here



    last_days = 7

    today = DT.date.today()
    med_sell_total = []
    date_total = []
    med_piece_total = []


    for det in range(last_days):
        week_ago = today - DT.timedelta(days=det)
        print(week_ago)
        date_total.append(week_ago)

        invoice_objects = Invoice.objects.filter(sellingDate=week_ago, is_sold=True)

        cost = 0.0
        for x in invoice_objects:
            medicine_invoice_objects = x.medicine_invoice.all()

            for y in medicine_invoice_objects:
                if y.medicine.medicineName == med_object.medicineName:
                    cost = cost + y.noOfUnit*y.medicine.price                               #Change needs here mandatory
        med_sell_total.append(cost)

        piece = 0
        for x in invoice_objects:
            medicine_invoice_objects = x.medicine_invoice.all()

            for y in medicine_invoice_objects:
                if y.medicine.medicineName == med_object.medicineName:
                    piece = piece + y.noOfUnit                              #Change needs here mandatory
        med_piece_total.append(piece)

    med_sell_total.reverse()
    date_total.reverse()
    print(med_sell_total)
    print(date_total)


    # End Here


    if request.is_ajax():
        typ = request.GET['type_of_ajax']

        if typ == "edit_limit":
            limitt = request.GET['post_med_id']
            # print("Inside ajax")
            # print(med_id)
            med_object.limit = limitt
            med_object.save()

        if typ == "edit_description":
            limitt = request.GET['post_med_id']
            # print("Inside ajax")
            # print(med_id)
            med_object.description = limitt
            med_object.save()

        if typ == "edit_cost":
            limitt = request.GET['post_med_id']
            # print("Inside ajax")
            # print(med_id)
            med_object.price = limitt
            med_object.save()

    return render(request, 'edit.html', {'limit': limit, 'cost': cost, 'cos': cos, 'descrip': descrip, 'here': here, 'date_total': date_total, 'med_sell_total': med_sell_total, 'med_piece_total': med_piece_total})



def Medicine_Reports(request):

    last_days = 7
    ratio = 0.1

    today = DT.date.today()
    med_sell_total = []
    date_total = []
    med_piece_total = []

    # for det in range(last_days):
    #     week_ago = today - DT.timedelta(days=det)
    #     print(week_ago)
    #     date_total.append(week_ago)

    invoice_objects = Invoice.objects.filter(sellingDate=today, is_sold=True)

    cost = 0.0
    for x in invoice_objects:
        medicine_invoice_objects = x.medicine_invoice.all()

        for y in medicine_invoice_objects:
            cost = cost + float(y.noOfUnit) * y.individual_price * 0.1 # Change needs here mandatory

    print(cost)

    return render(request, 'daily_med_reports.html', {})