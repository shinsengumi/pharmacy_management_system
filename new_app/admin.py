from django.contrib import admin

from .models import Medicine, GenericName, Branch, Employee, Salesman, MedicineInvoices, Invoice, BackOffice, Supply, MedicineBatch, Brand, MedicineTypeOnTake
admin.site.register(GenericName)
admin.site.register(Medicine)
admin.site.register(Branch)
admin.site.register(Employee)
admin.site.register(Salesman)
admin.site.register(MedicineInvoices)
admin.site.register(Invoice)
admin.site.register(BackOffice)
admin.site.register(Supply)
admin.site.register(MedicineBatch)
admin.site.register(Brand)
admin.site.register(MedicineTypeOnTake)



