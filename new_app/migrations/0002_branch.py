# Generated by Django 2.2.1 on 2019-09-14 14:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('new_app', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Branch',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('location', models.TextField(max_length=250)),
                ('telephoneNo', models.TextField(max_length=11)),
                ('mobileNo', models.TextField(max_length=11)),
                ('emailId', models.EmailField(max_length=110)),
            ],
        ),
    ]
