from django.db import models
from .utils import unique_slug_generator
from django.db.models.signals import pre_save, post_save, m2m_changed
from django.shortcuts import render, redirect
from datetime import datetime


class MedicineTypeOnTake(models.Model):
    intakeMethod = models.CharField(max_length=30, primary_key=True)

    def __str__(self):
        return self.intakeMethod


class Branch(models.Model):
    location = models.TextField(max_length=250, null=False, blank=False)
    telephoneNo = models.TextField(max_length=11, null=False, blank=False)
    mobileNo = models.TextField(max_length=11, null=False, blank=False)
    emailId = models.EmailField(max_length=110, null=False, blank=False)

    def __str__(self):
        return self.location


class Employee(models.Model):
    name = models.TextField(max_length=50, null=False, blank=False)
    address = models.TextField(max_length=250, null=False, blank=False)
    mobileNo = models.TextField(max_length=250, null=False, blank=False)
    emailId = models.EmailField(max_length=110, null=False, blank=False)
    joiningDate = models.DateField(null=False, blank=False)
    leavingDate = models.DateField(null=True, blank=True)
    password = models.TextField(max_length=250, null=True, blank=True)
    branch = models.ForeignKey(Branch, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Salesman(models.Model):
    counterNo = models.IntegerField(null=False, blank=False)
    employee = models.OneToOneField(Employee, on_delete=models.CASCADE, primary_key=True)

    def __str__(self):
        return self.employee.name


class GenericName(models.Model):
    genreName = models.CharField(max_length=250, unique=True)

    def __str__(self):
        return self.genreName


class Brand(models.Model):
    brand = models.TextField(null=False, blank=False)
    contactNo = models.TextField(null=True, blank=True)

    def __str__(self):
        return "%s" % self.brand


class Medicine(models.Model):
    medicineName = models.TextField(null=False, blank=False)
    slug = models.SlugField(blank=True, unique=True)
    genre = models.ForeignKey(GenericName, on_delete=models.CASCADE)
    medicineType = models.ForeignKey(MedicineTypeOnTake, on_delete=models.CASCADE)
    brand = models.ForeignKey(Brand, on_delete=models.CASCADE)
    # brand = models.CharField(max_length=30)
    limit = models.IntegerField(default=50)
    size = models.IntegerField(null=False, blank=False)
    amount = models.IntegerField(null=False, blank=False)
    price = models.FloatField(null=False, blank=False)
    recDose = models.TextField(null=False, blank=False)
    instructions = models.TextField(null=True, blank=True)
    description = models.TextField()


    def get_absolute_url(self):
        return "{slug}/".format(slug=self.slug)

    def __str__(self):
        return self.medicineName


class BackOffice(models.Model):
    designation = models.TextField()
    employee = models.OneToOneField(Employee, on_delete=models.CASCADE, primary_key=True)

    def __str__(self):
        return "%s %s" % (self.designation, self.employee.name)


class Supply(models.Model):
    time = models.TimeField(auto_now_add=True)
    brand = models.ForeignKey(Brand, on_delete=models.CASCADE)
    employee = models.ForeignKey(BackOffice, on_delete=models.CASCADE)

    def __str__(self):
        return "%s %s" % (self.time, self.brand)


class MedicineBatch(models.Model):
    noOfUnit = models.IntegerField(null=False, blank=False, default=0)
    buyingPrice = models.FloatField(null=False, blank=False)
    dateOfManufacture = models.DateField(null=True, blank=True)
    dateOfExpire = models.DateField(null=True, blank=True)
    medicine = models.ForeignKey(Medicine, on_delete=models.CASCADE)
    supply = models.ForeignKey(Supply, on_delete=models.CASCADE)


class MedicineInvoices(models.Model):
    medicine = models.ForeignKey(Medicine, on_delete=models.CASCADE)
    # batch = models.ForeignKey(MedicineBatch, on_delete=models.CASCADE)
    # invoice = models.ForeignKey(Invoice, on_delete=models.CASCADE)
    individual_price = models.FloatField(null=True, blank=True)
    noOfUnit = models.IntegerField(default=1)
    cond = models.BooleanField(default=False)

    @property
    def cost_calculator(self):
        return self.noOfUnit * self.medicine.price

    @property
    def profit_calculator(self):
        return self.noOfUnit * (self.medicine.price - self.batch.buyingPrice)

    def __str__(self):
        return str(self.id)


class Invoice(models.Model):
    sellingTime = models.TimeField(null=True, blank=True)
    sellingDate = models.DateField(null=True, blank=True)
    employee = models.ForeignKey(Salesman, on_delete=models.CASCADE)
    medicine_invoice = models.ManyToManyField(MedicineInvoices)
    selling_price = models.DecimalField(default=0.0, max_digits=100, decimal_places=2)
    is_sold = models.BooleanField(default=False)

    def __str__(self):
        return str(self.id)


########################################################################################################signal start


def medicine_pre_save_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = unique_slug_generator(instance)


pre_save.connect(medicine_pre_save_receiver, sender=Medicine)


def medicine_batch_post_save_reciever(sender, instance, *args, **kwargs):
    total = instance.medicine.amount + instance.noOfUnit
    instance.medicine.amount = total
    instance.medicine.save()


post_save.connect(medicine_batch_post_save_reciever, sender=MedicineBatch)

# def pre_save_cart_receiver(sender, instance, action, *args, **kwargs):
#     if action == 'post_add' or action == 'post_remove' or action == 'post_clear':
#         print("Hello there")
#         total = 0.0
#         medicine_obj = instance.medicine_invoice.all()    # medicine_obj is all m2m objects
#         for obj in medicine_obj:
#             check_med = Medicine.objects.get(medicineName=obj.medicine)  # Check_med is Medicine object
#             print(check_med.medicineName)
#
#             # for x in medicine:
#             total += check_med.price * obj.noOfUnit
#             # print(str(total))
#         instance.selling_price = total
#         instance.save()
#
#
# m2m_changed.connect(pre_save_cart_receiver, sender=Invoice.medicine_invoice.through)

########################################################################################################signal end
