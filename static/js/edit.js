$(document).ready(function () {


        var med_submit_form = $(".edit_limit");
        var med_description = $(".edit_description");
        var med_cost = $(".edit_cost");

        med_submit_form.submit(function (event) {
            event.preventDefault();
            var thisform = $(this);
            var hidden_fields = thisform.children('.form-group').children('#limit').val();
            var x = thisform.children('.form-group').children('#foo').val();


            var typ = "edit_limit";

            $.ajax(
            {
                type: "GET",
                url: "/edit/"+x,
                data: {
                    post_med_id: hidden_fields,
                    type_of_ajax: typ,
                },
                success: function (data) {

                }
            })

        });


        med_description.submit(function (event) {
            event.preventDefault();
            var thisform = $(this);
            var x = thisform.children('.form-group').children('.nxtt').children('#foo').val();
            var hidden_fields = thisform.children('.form-group').children('.nxtt').children('#des_area').val();

            var typ = "edit_description";

            $.ajax(
            {
                type: "GET",
                url: "/edit/"+x,
                data: {
                    post_med_id: hidden_fields,
                    type_of_ajax: typ,
                },
                success: function (data) {

                }
            })

        });


        med_cost.submit(function (event) {
            event.preventDefault();
            var thisform = $(this);
            var hidden_fields = thisform.children('.form-group').children('#cost').val();
            var x = thisform.children('.form-group').children('#foo').val();

            var typ = "edit_cost";

            $.ajax(
            {
                type: "GET",
                url: "/edit/"+x,
                data: {
                    post_med_id: hidden_fields,
                    type_of_ajax: typ,
                },
                success: function (data) {

                }
            })

        });

    });